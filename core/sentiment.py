import os
import json
import string
import re
from .models import *


class Sentiment:
    dataFolder = ''
    ignoreList = []
    listKata = {}
    negPrefixList = []
    dictionary = {'positif': {}, 'negatif': {}, 'netral': {}}
    minTokenLength = 1
    maxTokenLength = 15
    category = ['positif', 'negatif', 'netral']
    catTokCounts = {'positif': 0, 'negatif': 0, 'netral': 0}
    catDocCounts = {'positif': 0, 'negatif': 0, 'netral': 0}

    tokCount = 0
    docCount = 0

    prior = {'positif': 0.333, 'negatif': 0.333, 'netral': 0.334}

    def __init__(self):
        self.dataFolder = os.path.abspath(os.path.dirname(__file__)) + "/data/"
        self.loadDefaults()

    def loadDefaults(self):
        for cat in self.category:
            self.setDictionary(cat)
        self.ignoreList = self.getList('ignore')
        self.negPrefixList = self.getList('prefix')

    def setDictionary(self, category):
        with open(self.dataFolder + category + '.json') as file:
            data = json.load(file)
        for word in data.get(category):
            self.docCount += 1
            self.catDocCounts[category] += 1
            word = word.strip()
            self.dictionary[category][word] = 1
        for word in Word.objects.filter(sentiment=category):
            self.docCount += 1
            self.catDocCounts[category] += 1
            word = word.word.strip()
            self.dictionary[category][word] = 1

        self.catTokCounts[category] += 1
        self.tokCount += 1

    def getList(self, category):
        wordList = []
        with open(self.dataFolder + category + '.json') as file:
            data = json.load(file)
        for word in data.get(category):
            word = word.strip()
            wordList.append(word)
        for word in Word.objects.filter(sentiment=category):
            word = word.word.strip()
            wordList.append(word)
        return wordList

    def score(self, sentence):
        # Make dictionary for category per word
        for token in self._getTokens(sentence):
            if token in self.negPrefixList:
                self.listKata.update({token: "netral"})
                continue
            elif token in self.ignoreList:
                self.listKata.update({token: "ignore"})
                continue
            for cat in self.category:
                if self.dictionary.get(cat).get(token) != None:
                    self.listKata.update({token: cat})
                    break
            else:
                self.listKata.update({token: None})

        # Handle prefix (remove whitespace from after the word)
        # ex. tidak bagus => tidakbagus
        for negPrefix in self.negPrefixList:
            insensitive_negPrefix = re.compile(re.escape(negPrefix+" "), re.IGNORECASE)
            sentence = insensitive_negPrefix.sub(negPrefix, sentence)

        tokens = self._getTokens(sentence)

        # Scoring algorithm
        total_score = 0
        scores = {}
        for cat in self.category:
            scores[cat] = 1

            for token in tokens:
                if len(token) > self.minTokenLength and len(
                        token) < self.maxTokenLength and token not in self.ignoreList:
                    if self.dictionary.get(cat).get(token) != None:
                        count = self.dictionary[cat][token]
                    else:
                        count = 0
                    scores[cat] *= count + 1
            scores[cat] = self.prior[cat] * scores[cat]

        for cat in self.category:
            total_score += scores[cat]
        for cat in self.category:
            scores[cat] = round(scores[cat] / total_score, 3)
        return scores

    def categorize(self, sentence):
        self.listKata = {}
        s = ""
        for char in str(sentence):
            if char not in string.punctuation:
                s = s + char
        scores = self.score(s)
        negatif = scores['negatif']
        positif = scores['positif']
        netral = scores['netral']
        if (negatif > positif) and (negatif > netral):
            return 'negatif'
        elif (positif > negatif) and (positif > netral):
            return 'positif'
        elif (positif == negatif):
            return 'netral'
        classification = max(scores, key=scores.get)
        return classification

    def _getTokens(self, string):
        string = string.replace('\n', ' ').lower()
        matches = string.split(' ')
        return matches

