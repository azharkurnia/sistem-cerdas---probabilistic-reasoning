from django.conf.urls import url
from .views import *
#url for app

app_name = "core"
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^predict/$', predict, name='predict'),
    url(r'^score/$', score, name='score'),
]
