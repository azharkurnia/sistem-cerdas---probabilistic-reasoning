from django import forms

# Tweet Form
class TweetForm(forms.Form):
    tweet = forms.CharField(widget = forms.Textarea, label='Your tweet', max_length=140)