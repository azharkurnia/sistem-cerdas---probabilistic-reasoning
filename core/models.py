from django.db import models

POSITIVE = "positif"
NEGATIVE = "negatif"
NEUTRAL = "netral"

SENTIMENT = (
    (POSITIVE, "positif"),
    (NEGATIVE, "negatif"),
    (NEUTRAL, "netral")
)

# Create your models here.
class Word(models.Model):
    word = models.CharField(max_length=140)
    sentiment = models.CharField(choices=SENTIMENT, max_length=8, default=NEUTRAL)

    def __str__(self):
        return self.word + " (" + self.sentiment + ")"