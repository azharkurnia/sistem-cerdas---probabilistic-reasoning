from django.shortcuts import render
from .sentiment import *
from .models import *
import string


# Create your views here.
def index(request):
    response = {"last_five": Word.objects.all().order_by('-id')[:5]}
    print(response)
    return render(request, 'index.html', response)

def predict(request):
    response = {"last_five": Word.objects.all().order_by('-id')[:5]}
    sentimentChecker = Sentiment()
    if request.method == 'POST':
        tweet = request.POST['tweet']
        if tweet != '':
            response.update({'tweet': tweet})
            result = sentimentChecker.categorize(tweet)
            if (result == 'positif'):
                response.update({'result': 'positif'})
            elif (result == 'negatif'):
                response.update({'result': 'negatif'})
            elif (result == 'netral'):
                response.update({'result': 'netral'})
            listKata = sentimentChecker.listKata
            klasifikasikata = {}

            cleartweet = ""
            for char in tweet:
                if char not in string.punctuation:
                    cleartweet += char

            for kata in cleartweet.split(' '):
                if kata.lower() in listKata:
                    klasifikasikata.update({kata: listKata[kata.lower()]})
            response.update({'klasifikasi': klasifikasikata})

    return render(request, 'index.html', response)

def score(request):
    response = {"last_five": Word.objects.all().order_by('-id')[:5]}
    sentimentChecker = Sentiment()
    if request.method == 'POST':
        word = request.POST['word']
        sentiment = request.POST['sentiment']
        new_word = Word.objects.create(word=word, sentiment=sentiment)

        tweet = request.POST['tweet']
        if tweet != '':
            response.update({'tweet': tweet})
            result = sentimentChecker.categorize(tweet)
            if (result == 'positif'):
                response.update({'result': 'positif'})
            elif (result == 'negatif'):
                response.update({'result': 'negatif'})
            elif (result == 'netral'):
                response.update({'result': 'netral'})
            listKata = sentimentChecker.listKata
            klasifikasikata = {}

            cleartweet = ""
            for char in tweet:
                if char not in string.punctuation:
                    cleartweet += char

            for kata in cleartweet.split(' '):
                if kata.lower() in listKata:
                    klasifikasikata.update({kata: listKata[kata.lower()]})
            response.update({'klasifikasi': klasifikasikata})

    return render(request, 'index.html', response)