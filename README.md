# Probabilistic Review 1.0
Also available at http://probabilistic-review.herokuapp.com
by sestim cardes - Ahmad Satryaji Aulia, Azhar Kurnia, Kianutama Radianur

## Getting started
* Installation*
Install once globally:
```
  pip install -r requirements.txt
```

* Run development server *
```
  python manage.py migrate
  python manage.py runserver
```